<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Folders List</title>
</head>
<body>

<h2>User info</h2>
 <h6><a href="j_spring_security_logout">Click here to logout</a></h6>
 <sec:authorize access="isAuthenticated()">
 Username: <sec:authentication property="principal.username" />
 Role: <sec:authentication property="principal.authorities"/>
 </sec:authorize>

<h3>Folders</h3>
<c:if  test="${!empty folders}">
<table class="data">
<tr>
	
    <th>Name</th>
    <th>&nbsp;</th>
</tr>
<c:forEach items="${folders}" var="folder">
    <tr>   		
        <td>${folder.name} </td>
    </tr>
</c:forEach>
</table>
</c:if>

</body>
</html>