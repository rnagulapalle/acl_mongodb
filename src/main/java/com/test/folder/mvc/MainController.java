package com.test.folder.mvc;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.test.folder.domain.Folder;
import com.test.folder.service.FolderService;


/**
 *
 * @author eljemli.anasse
 */
@Controller
public class MainController {
	
	@Autowired
	private FolderService folderService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String defaultPage(ModelMap map) {
		return "redirect:/list";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		return "login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
		return "logout";
	}
	
	@RequestMapping(value = "/accessdenied")
	public String loginerror(ModelMap model) {
		model.addAttribute("error", "true");
		return "denied";
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listCampaigns(ModelMap map) {
		
		Iterable<Folder> folders= folderService.getAllFolder();
		map.addAttribute("folders",folders);
		
		return "list";
	}
	
}