package com.test.folder.repositories;

import org.springframework.data.repository.CrudRepository;

import com.test.folder.domain.Folder;

public interface FolderRepositoryDao extends CrudRepository<Folder, String> {
	
	
//	@PostFilter(value = "")
//	public List<Campaign> findByAll();

}