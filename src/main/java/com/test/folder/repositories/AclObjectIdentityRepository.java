package com.test.folder.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.test.folder.domain.AclObjectIdentity;


public interface AclObjectIdentityRepository extends CrudRepository<AclObjectIdentity, String> {

	
	AclObjectIdentity findByObjectIdIdentity(ObjectId d);
	
	@Query("{'objectIdClass' : {'$ref': 'acl_class', '$id': ?0 },'objectIdIdentity' : ?1 ,'ownerId' : {'$ref': 'users', '$id': ?2 } }")
	AclObjectIdentity isOwnerOfObject(ObjectId classId, ObjectId ObjectId, ObjectId ownerId);
}
