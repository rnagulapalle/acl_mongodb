package com.test.folder.service;

public interface PermissionService {


	boolean hadAccess(String className, String EntityId, String authentication);

}
