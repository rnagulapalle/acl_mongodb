package com.test.folder.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.stereotype.Service;

import com.test.folder.domain.Folder;
import com.test.folder.repositories.FolderRepositoryDao;

@Service(value="folderService")
public class FolderServiceImpl implements FolderService {

	
	@Autowired
	private  FolderRepositoryDao campaignRepositoryDao;
	
	
	@Override
	@PostFilter("@permissionService.hadAccess( 'com.test.folder.domain.Folder',filterObject.id,authentication.name) or  hasRole('ROLE_ADMIN')")
	public Iterable<Folder> getAllFolder() {
		return campaignRepositoryDao.findAll();
	}
	
}
